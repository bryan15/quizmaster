/*
 * Copyright (c) Bryan Aaldering 2018.
 */

package nl.bryanaaldering.quizmaster;

import org.junit.Test;
import static org.junit.Assert.*;


public class PlayerTest {

    @Test
    public void shouldHaveName(){
        Player player = new Player();
        player.setName("Bryan");
        assertNotNull(player.getName());
    }

    @Test
    public void shouldHaveNameBryan(){
        Player player = new Player();
        player.setName("Bryan");
        assertEquals("Bryan", player.getName());
    }
}
