/*
 * Copyright (c) Bryan Aaldering 2018.
 */

package nl.bryanaaldering.quizmaster;
import name.johannestextor.cstools.SimpleIO;
import org.junit.Test;
import static org.junit.Assert.*;

public class ImportQATest {

    @Test
    public void shouldImportQuestionsFile(){
        ImportQuestionsAndAnswers importQuestions = new ImportQuestionsAndAnswers();
        String questionsFile = "./questions";
        importQuestions.setQuestionsFile(SimpleIO.readFile(questionsFile));
        assertNotNull(importQuestions);
    }

    @Test
    public void shouldImportAnswersFile(){
        ImportQuestionsAndAnswers importAnswers = new ImportQuestionsAndAnswers();
        String answersFile = "./answers";
        importAnswers.setAnswersFile(SimpleIO.readFile(answersFile));
        assertNotNull(importAnswers);
    }

    @Test
    public void shouldRecognizeQuestionsFileHas8Lines(){
        ImportQuestionsAndAnswers importQuestions = new ImportQuestionsAndAnswers();
        String questionsFile = "./questions";
        importQuestions.setQuestionsFile(SimpleIO.readFile(questionsFile));
        assertEquals(8, importQuestions.getQuestionsFile().length);
    }
    @Test
    public void shouldRecognizeTheRightAnswerToTheCorrespondingQuestion() {
        ImportQuestionsAndAnswers importQA = new ImportQuestionsAndAnswers();
        String questionsFile = "./questions";
        String answersFile = "./answers";
        importQA.setQuestionsFile(SimpleIO.readFile(questionsFile));
        importQA.setAnswersFile(SimpleIO.readFile(answersFile));
        int q6 = 5;
        assertEquals("true", importQA.getAnswersFile()[q6]);
    }
}
