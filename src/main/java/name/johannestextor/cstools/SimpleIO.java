/*
 * Copyright (c) Bryan Aaldering 2018.
 */

/*
 * Created on 05.03.2009 by Johannes Textor
 * This Code is licensed under the BSD license:
 * http://www.opensource.org/licenses/bsd-license.php
 */
package name.johannestextor.cstools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class SimpleIO {
	public static String readLine() {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		try {
			return in.readLine();
		} catch (IOException e) {
			return "";
		}

	}

	public static String[] readFile(String name) {
		ArrayList<String> zeilen = new ArrayList<String>();
		String[] r = null;
		try {
			FileInputStream fstream = new FileInputStream(name);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				zeilen.add(strLine);
			}
			in.close();
			r = new String[zeilen.size()];
			zeilen.toArray(r);
		} catch (Exception e) {
			System.err.println("Error opening file: "
					+ e.getMessage());
			System.err
					.println("Is the file in the right folder? Is the filename written correctly?");
			try {
				System.err.println("The current folder is: "
						+ (new File(".")).getCanonicalPath());
			} catch (IOException e1) {
			}
		} finally {
		}
		return r;
	}

	public static void writeToFile( String name, String[] data ){
		writeToFile(name, data, false);
	}
	
	public static void appendToFile( String name, String[] data ){
		writeToFile(name, data, true);
	}
	
	private static void writeToFile( String name, String[] data, boolean append ){
		try {
			PrintWriter bw = new PrintWriter( new FileWriter( name, append ) );
			for( int i = 0 ; i < data.length ; i ++ ){
				bw.println(data[i]);
			}
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("Error writing to file!");
			e.printStackTrace();
		}
	}
}

