/*
 * Copyright (c) Bryan Aaldering 2018.
 */

package nl.bryanaaldering.quizmaster;

import name.johannestextor.cstools.SimpleIO;
import java.util.ArrayList;

class LetsPlay {
    static void play(int questionsLength, String[] questions, String[] answers, String name){

		System.out.println(name + ", how many questions do you want? \n");
		try {
			int numberOfQuestionsWanted = Integer.parseInt(SimpleIO.readLine());

			if(numberOfQuestionsWanted > questionsLength || numberOfQuestionsWanted < 1){
				System.err.println("Please choose a number between 1 and " + questionsLength);
				play(questionsLength, questions, answers, name);
			}

			System.out.println("I will now show you " + numberOfQuestionsWanted + " statements.");
			System.out.println("You need to answer true or false." );

			int countCorrectNumberQuestions = 0;

			int questionsAsked = 0;
			ArrayList<Integer> listQuestionsAsked = new ArrayList<>();

			while (questionsAsked < numberOfQuestionsWanted){
				int randomNumber = RandomNumGen.randomNumber(questionsLength);
				if(listQuestionsAsked.contains(randomNumber)) {
					continue;
				} else {
					listQuestionsAsked.add(randomNumber);
				}

				System.out.println(questions[randomNumber]);
				System.out.println("Please enter true or false \n");
				String userAnswer = SimpleIO.readLine();
				String userAnswerLowerCase = userAnswer.toLowerCase();

				if(userAnswerLowerCase.equals(answers[randomNumber])) {
					countCorrectNumberQuestions++;
					System.out.println("Correct answer! \n");
				} else {System.out.println("Wrong answer... :( The correct answer is "+ answers[randomNumber]);}
				questionsAsked++;
			}

			System.out.println("Hey "+ name + "! You've scored "+ countCorrectNumberQuestions+ " out of "+numberOfQuestionsWanted+ "!\n");
			if(countCorrectNumberQuestions > numberOfQuestionsWanted*0.5){
				System.out.println("You have passed!\n");
			} else {
				System.out.println("You needed more than 50% to pass, and so just like Gandalf said 'You shall not pass!'\n");
			}

		} catch (NumberFormatException e) {
			System.err.println("You didn't enter an integer. Please try again!");
			play(questionsLength, questions, answers, name);
		}

	}
}
