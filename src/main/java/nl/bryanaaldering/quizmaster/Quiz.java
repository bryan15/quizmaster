/*
 * Copyright (c) Bryan Aaldering 2018.
 */

package nl.bryanaaldering.quizmaster;

import name.johannestextor.cstools.SimpleIO;

/**
 * Created on 2017-10-24 by Bryan Aaldering
 * Lets the user participate in a quiz, played in the console
 * Questions and matching answers must be provided in 2 separate text files, with each question/answer on a new line.
 *
 * @author Bryan Aaldering
 * @version 1.0
 */
public class Quiz {

    public static void main(String[] args) {
        // create new player and say hi!
        Player player = new Player();
        ImportQuestionsAndAnswers importQuestionsAndAnswers = new ImportQuestionsAndAnswers();
        Quiz.greeter();

        //set players name
        player.setName(SimpleIO.readLine());

        //greet the player
        System.out.println("Hey " + player.getName() + " let's play a game!");

        //import questions and answers
        System.out.println("Checking the folder for the questions-file ... \n");
        String nameQuestionsFile = "questions";
        importQuestionsAndAnswers.setQuestionsFile(SimpleIO.readFile(nameQuestionsFile));
        System.out.println("Found it! There are " + importQuestionsAndAnswers.getQuestionsFile().length + " questions in this file! \n");
        System.out.println("Checking the folder for anwsers-file... \n");
        String nameAnswersFile = "answers";
        System.out.println("Found it! \n");

        importQuestionsAndAnswers.setAnswersFile(SimpleIO.readFile(nameAnswersFile));

        //ask the questions
        LetsPlay.play(importQuestionsAndAnswers.getQuestionsFile().length, importQuestionsAndAnswers.getQuestionsFile(), importQuestionsAndAnswers.getAnswersFile(), player.getName());
    }

    private static void greeter(){
        System.out.println("Hello! Please enter your name: \n");
    }

}
