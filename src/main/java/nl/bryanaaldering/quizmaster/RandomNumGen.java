/*
 * Copyright (c) Bryan Aaldering 2018.
 */

package nl.bryanaaldering.quizmaster;

import java.util.Random;

/**
 * Created on 2017-10-24 by Bryan Aaldering
 * Generates a random number up to the lenght (number of lines) of the file imported in nl.bryanaaldering.quizmaster.Quiz.java
 * It then returns a random number in the form of an integer
 *
 * @author Bryan Aaldering
 * @version 1.0
 */

class RandomNumGen {

    static int randomNumber(int lengthQuestionsFile){
        Random rand = new Random();

        int randInt = rand.nextInt(lengthQuestionsFile);

        return randInt;
    }
}


